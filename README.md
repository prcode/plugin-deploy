# README #

Rapid Resizer plugin deployment command.

### What is this repository for? ###

Deploy plugins onto the platform.

### How do I get set up? ###

Install it:
```console
npm i -g bitbucket:prcode/plugin-deploy
```

Deploy the plugin in the current directory:
```console
plugin-deploy
```
Assumes the plugin's plugin.zip (containing manifest.json, plugin.html, plugin.js, etc.) and manifest.json files are in the current directory.

### Who do I talk to? ###

Email help@rapidresizer.com