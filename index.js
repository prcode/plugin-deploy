#!/usr/bin/env node

import fs from 'fs';
import fetch from 'node-fetch';
import { createHash } from 'crypto';

const api = 'https://od8ipm3wxa.execute-api.us-east-1.amazonaws.com/production/uploadimage';

const zipPath = fs.existsSync('dist/plugin.zip') ? 'dist/plugin.zip' : 'plugin.zip';
fs.createReadStream(zipPath).pipe(createHash('sha256')).on('finish', async function() {
    const hash = this.read().toString('hex');
    let response = await fetch(api, {
        method: 'POST',
        headers: {
            Referer: 'https://online.rapidresizer.com/'
        },
        body: new URLSearchParams({
            getPutUrl: hash,
            contentType: 'application/zip'
        })
    });
    const putUrl = (await response.json()).url;
    console.log('Uploading to', putUrl);

    response = await fetch(putUrl, {
        method: 'PUT',
        body: fs.readFileSync(zipPath),
        headers: {
            'Content-Type': 'application/zip',
            Referer: 'https://online.rapidresizer.com/'
        }
    });
    if (response.status != 200) {
        console.log('Upload failed');
        process.exit(1);
    }
    console.log('Uploaded zip file');

    response = await fetch(api, {
        method: 'POST',
        headers: {
            Referer: 'https://online.rapidresizer.com/'
        },
        body: new URLSearchParams({
            authorization: process.env.ADD_PLUGIN_KEY,
            platform: 'add',
            id: hash,
            metadata: fs.readFileSync('manifest.json').toString()
        })
    });
    if (response.status == 200) {
        console.log(await response.text());
    } else {
        console.log(`ERROR: Failure adding plugin to database; HTTP status code ${response.status}`);
        process.exit(1);
    }
});